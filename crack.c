#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is,  return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int len = 0;
    // Hash the guess using MD5
    len = strlen(guess);
    char *hasher = md5(guess, len);
    
    // Compare the two hashes
    int comp = strcmp(hasher, hash);
    if(comp == 0)
    {
        free(hasher);
        return 1;
    }
    else
    {
        free(hasher);
        return 0;
    }
    // Free any malloc'd memory
    
}

int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
    {
        return -1;
    }
    else
    {
        return fileinfo.st_size;
    }
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    char *file_contents = malloc(len);
    
    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("Coundn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    int line_count = 0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    char **lines = malloc(line_count * sizeof(char *));
    
    int size_dlen = 0;
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        while (file_contents[c] != '\0')
        {
            c++;
        }
        c++;
        size_dlen ++;
    }
    
    *size = size_dlen;
    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[1]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen = 0;
    char **dict = read_dictionary(argv[2], &dlen);
    
    
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    int guessing = 0;
    char hash[HASH_LEN + 1];
    while(fgets(hash, HASH_LEN +1, fp) != NULL)
    {
        hash[strlen(hash)-1] = '\0';
        for(int i = 0; i < dlen; i++)
        {
            
            guessing = tryguess(hash, dict[i]);
            
            if(guessing == 1)
            {
                printf("Cracked %s it was %s\n", hash, dict[i]);
                break;
            }
        }
    }
    
    /*for(int n = 0; n < dlen; n++)
    {
        printf("%d %s\n", n, dict[n]);
    }*/
    
    fclose(fp);
    free(dict[0]);
    free(dict);
    
    // Open the hash file for reading.
    
    

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
}
